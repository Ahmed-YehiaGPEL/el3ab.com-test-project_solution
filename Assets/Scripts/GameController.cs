﻿using System;
using UnityEngine;
using System.Collections;
using System.Globalization;
using JetBrains.Annotations;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    /*
	Just in case the git ignored the tag manager
		A tag with the name 'CenterVec' has been added and currently assigned to the 'Sort' button game object
	*/
    /// <summary>
    /// # el3ab internship test
    /// 
    ///   Write code to sortd the circles "children's under gameobject "container" in cirlce form around sort btn when user click on sort button
    /// 
    ///   NOTE : - feel free to change the following code as you see fit if you want 
    ///          - keep you code organized and commented
    /// 
    ///     Good Luck 
    ///   
    /// </summary>

    //container is filled with small circles 
    //  small circles is the game object's  that shall be sorted in form of circle shape around the Button
    public GameObject container;

    [Tooltip("If true uses the default center point of the object tagged 'CenterVec' (currently 'Sort' button) in center screen.")]
    public bool UseDefCentPoint = false;

    [Tooltip("The text control to show the current Radius value on.")]
    public UnityEngine.UI.Text IndicatorText; public GameObject CenterPointGameObject;
    [Tooltip("The slider control from which user controls the radius of the circle.")]
    [SerializeField] private Slider _radiusControlSlider;

    /// <summary>
    /// The circle radius used in drawing, modified by the slider.
    /// </summary>
    private float _circleRadius;
    private void Awake()
    {
        //First intialize values

        _circleRadius = _radiusControlSlider.minValue;
        IndicatorText.text = _circleRadius.ToString();
    }
    // onClick on ("sort button") this function will be called and excuted
    public void SortBtn() 
    {
        //loop to scan each child inside the container
        for (int i = 0; i < container.transform.childCount; i++)
        {
            //  set child position depending on it's index 
            container.transform.GetChild(i).transform.localPosition = calculatePosition(i,container.transform.childCount);
        }
    }
    // function responsible to calculate position 
    // Write your Code Here  
    //VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
    private Vector2 calculatePosition(int index, int pointsCount)
    {
        /* 
        var used for simplfying system types 
        
        Main Formula 
          (x,y) = (Cx + sin(t) * r , Cy + cos(t)*r)  | Cx , Cy Center Point Coordinates

        */

        var i = (index * 1.0) / pointsCount; //Current index multipled by 1.0 to assure fraction preservation, i represents offset of the angle in circle
        var angle = i * Mathf.PI * 2; // PI * 2 -> Full Circle in Radians
        
        //Calculate sin(t) * r , cos(t) * r
        var x = Mathf.Sin((float)angle) * _circleRadius;
        var y = Mathf.Cos((float)angle) * _circleRadius;

        var pos = ((!UseDefCentPoint) // checking whether to use the default center point and choosing the correct game object.
            ? CenterPointGameObject.GetComponent<RectTransform>().localPosition
            : GameObject.FindGameObjectWithTag("CenterVec").GetComponent<RectTransform>().localPosition)
            + new Vector3(x, y, 0); // then add the remaining vector according to formula , Line 62

        return new Vector2(pos.x, pos.y); // here it's 
    }
    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    /// <summary>
    /// Slide value change event, raised when value changes.
    /// </summary>
    /// <param name="value">Current value of the slider</param>
    public void OnValueChange(float value)
    {
        _circleRadius = value;
        IndicatorText.text = value.ToString(CultureInfo.CurrentCulture);
    }
    // NOTE AGAIN : you have permission to change code as you see fit if you want 
}
