﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Debug = System.Diagnostics.Debug;
/// <summary>
/// Overrides the default inspector of Game Controller Script.
/// </summary>
[CustomEditor(typeof(GameController))]
public class GameControllerEditor : Editor {
    public override void OnInspectorGUI()
    {
        var tController = target as GameController;
        DrawProperty("container", "Circles Container");
        DrawProperty("IndicatorText","Radius Value Indicator");
        DrawProperty("UseDefCentPoint", "Use Def.Center Point");
        if (!tController.UseDefCentPoint)
        {
            DrawProperty("CenterPointGameObject", "Center Point Object");
            if(tController.CenterPointGameObject.GetComponent<RectTransform>() == null)
                EditorGUILayout.HelpBox("Center Point Game Object Must Contain a RectTransform Component and within a Canvas Control.",MessageType.Error);
            else
                EditorGUILayout.HelpBox("This object local position vector of component RectTransform will be used as a center point for drawing.", MessageType.Info);
        }
        DrawProperty("_radiusControlSlider", "Radius Control Slider");

        EditorGUILayout.HelpBox("You can change between using the default point around the button, or assigning a new center point game object.",MessageType.None);
        serializedObject.ApplyModifiedProperties();
    }
    /// <summary>
    /// Draws a property by name and a label from a serialized object, first level draw only.
    /// </summary>
    /// <param name="propertyName">The property to locate in the serialized object.</param>
    /// <param name="label">Label shown in the inspector.</param>
    private void DrawProperty(string propertyName, string label)
    {
        EditorGUILayout.PropertyField(serializedObject.FindProperty(propertyName), new GUIContent(label));
    }
}
